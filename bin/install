#!/usr/bin/env bash

# Exit on failure
set -e

flavour='unknown'
unamestr=$(uname)

if [[ "$unamestr" == 'Linux' ]]; then
    flavour='perso'
else
    printf "Unknown plateform!\n"
    exit 1
fi

DOTFILES_DIRECTORY=~/.dotfiles
DOTFILES_TARBALL_PATH="https://framagit.org/arnaudfoisset/dotfiles/-/archive/$flavour/dotfiles-$flavour.tar.gz"
DOTFILES_GIT_HTTPS="https://framagit.org/arnaudfoisset/dotfiles.git"
DOTFILES_GIT_SSH="git@framagit.org:arnaudfoisset/dotfiles.git"

# If missing, download and extract the dotfiles repository
if [[ ! -d $DOTFILES_DIRECTORY ]]; then
    printf "Downloading dotfiles...\n"
    mkdir $DOTFILES_DIRECTORY
    # Get the tarball
    wget -O ~/dotfiles.tar.gz $DOTFILES_TARBALL_PATH
    # Extract to the dotfiles directory
    tar -zxf ~/dotfiles.tar.gz --strip-components 1 -C $DOTFILES_DIRECTORY
    # Remove the tarball
    rm -rf ~/dotfiles.tar.gz
fi

cd $DOTFILES_DIRECTORY

source ./lib/apt

source ./lib/pip
source ./lib/configure

source ./shell/bash.d/colors
source ./shell/bash.d/exports
source ./shell/bash.d/utils

run_help() {
  cat <<EOT
Debian and Ubuntu dotfiles

Usage: $(basename "$0") [options]

Options:
    -h, --help      Print this help text
    --no-packages   Suppress package updates
    --no-sync       Suppress pulling from the remote repository

Documentation can be found at https://framagit.org/arnaudfoisset/dotfiles

Copyright (c) Hugo Rodde
Licensed under the MIT license.
EOT
}

# Options
for opt in $@; do
    case $opt in
        --no-packages) no_packages=true ;;
        --no-sync) no_sync=true ;;
        -h|--help) run_help; exit 0 ;;
        -*|--*) e_warning "Error: invalid option $opt"; run_help; exit 1 ;;
    esac
done

# Prepare /usr/local directory
mkdir -p /usr/local
sudo chown -R $(whoami) /usr/local

init_git() {
    # Bootstrap git
    if ! type "git" > /dev/null; then
        apt_install_git
    fi

    # Initialize the git repository if it's missing
    if ! is_git_repository; then
        e_header "Initializing git repository"
        git init
        git remote add read-only $DOTFILES_GIT_HTTPS
        git remote add origin $DOTFILES_GIT_SSH
        git fetch read-only $flavour
        # Reset the index and working tree to the fetched HEAD
        # (submodules are cloned in the subsequent sync step)
        git reset --hard FETCH_HEAD
        # Remove any untracked files
        git clean -fd
        # Checkout to the $flavour branch
        git checkout $flavour
        # Remove useless master branch
        git branch -D master
    fi
}

sync_remote() {
    # Conditionally sync with the remote repository
    if [[ $no_sync ]]; then
        e_info "Skipped dotfiles sync."
    else
        e_header "Syncing dotfiles"
        # Stash working directory just in case
        git stash
        # Pull down the latest changes
        git pull --rebase read-only $flavour
        # Update submodules
        git submodule update --recursive --init --quiet
    fi
}

install_packages()
{
    if [[ $no_packages ]]; then
        e_info "Skipped packages installation."
    else
        run_softwares

        # Install oh-my-zsh
        sh -c "$(wget -O- https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended
        fi
}

link() {
    # Force create/replace the symlink.
    ln -fs $DOTFILES_DIRECTORY/$1 ~/$2
}

pre_mirrorfiles() {
    # Do the same for vim
    [[ ! -d ~/.vim/undo ]]    && mkdir -p ~/.vim/undo
    [[ ! -d ~/.vim/swaps ]]   && mkdir -p ~/.vim/swaps
    [[ ! -d ~/.vim/colors ]]  && mkdir -p ~/.vim/colors
    [[ ! -d ~/.vim/bundle ]]  && mkdir -p ~/.vim/bundle
    [[ ! -d ~/.vim/backups ]] && mkdir -p ~/.vim/backups

    if [[ ! -d ~/.vim/bundle/Vundle.vim ]]; then
      git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
    fi
}

mirrorfiles() {
    # Create the necessary symbolic links between the `.dotfiles` and `HOME`
    # directory. The `bash_profile` sources other files directly from the
    # `.dotfiles` repository.
    link "terminator"                   ".config/terminator"
    link "editor/editorconfig"          ".editorconfig"
    link "editor/editorthemes"          ".editorthemes"
    link "git/gitconfig"                ".gitconfig"
    link "git/gitignore"                ".gitignore"
    link "shell/zshrc"                    ".zshrc"
    link "shell/bash.d"                 ".bash.d"
    link "shell/bash_profile"           ".bash_profile"
    link "shell/bash_profile.private"   ".bash_profile.private"
    link "shell/bashrc"                 ".bashrc"
    link "shell/curlrc"                 ".curlrc"
    link "shell/wgetrc"                 ".wgetrc"
    link "vim/vimrc"                    ".vimrc"
    link "editor/editorthemes/tomorrow-night/vim/Tomorrow-Night.vim" ".vim/colors/Tomorrow-Night.vim"
}

post_mirrorfiles() {
    # Install vim plugins
    if type_exists 'vim'; then
        vim +PluginInstall +qall
        ln -sf ~/.vim/{bundle/vim-colorschemes/,}colors
    fi

    # Change to the new shell
    chsh -s $(which zsh)

    # Finally source bash_profile
    source ~/.bash_profile
}

# Ask before potentially overwriting files
seek_confirmation "Warning: This step may overwrite your existing dotfiles. Continue?"

if is_confirmed; then
    init_git
    sync_remote
    install_packages
    pre_mirrorfiles
    mirrorfiles
    post_mirrorfiles
    e_success "Dotfiles installation completed!"
else
    e_error "Aborting..."
    exit 1
fi

