# dotfiles [![Build Status](https://jenkins.favrodd.com/job/dotfiles/badge/icon)](https://jenkins.favrodd.com/job/dotfiles/)

### OS X

```bash
wget -O install.sh https://framagit.org/arnaudfoisset/dotfiles/-/raw/perso/bin/install
chmod +x install.sh
./install.sh
rm install.sh
```
